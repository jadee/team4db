import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.sql.*;
public class CreateTransactions extends TransactionBase
{
     private final String createtablesqlstatement = 
     	"CREATE TABLE transactions( " +
		"op_id int(11) NOT NULL," +
		"buyer varchar(140) NOT NULL," +
		"seller varchar(140) NOT NULL," +
		"trans_date date NOT NULL," +
		"exercise_date date NOT NULL," +
		"stock_price decimal (10, 2)," +
		"option_type varchar(7)," +
		"PRIMARY KEY(buyer, seller, trans_date)," +
		"FOREIGN KEY(op_id) REFERENCES options_detail(op_id)" +
		")";
	private String  CheckWeekend(SimpleDateFormat sdf, String date) throws ParseException
     {
     		Calendar c = Calendar.getInstance();
     		c.setTime(sdf.parse(date));
     		Integer dayofWeek = c.get(Calendar.DAY_OF_WEEK);
     		if(dayofWeek == 1) {c.add(Calendar.DATE,1); return sdf.format(c.getTime());}
     		else if(dayofWeek == 7) {c.add(Calendar.DATE, 2); return sdf.format(c.getTime());}
     		else {return date;}
     }
     private String CheckDate(String date) throws ParseException
     {
     		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
     		Date lowerbound = sdf.parse("2005-02-08");
     		Date upperbound = sdf.parse("2005-10-14");
     		Date the_date = sdf.parse(date);
     		if(the_date.compareTo(lowerbound) < 0) {return sdf.format(lowerbound);}
     		if(the_date.compareTo(upperbound) > 0) {return sdf.format(upperbound);} //We wish for our expiration dates to be 30 days apart from the original. This is the max date we'll need. 
     		return CheckWeekend(sdf, date);
     		
     }
     private java.sql.Date AddExpTime(java.sql.Date date, Integer interval) throws ParseException
     {
     		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
     		Calendar c = Calendar.getInstance();
     		c.setTime(date);
     		c.add(Calendar.DATE, interval);
     		return java.sql.Date.valueOf(CheckDate(sdf.format(c.getTime())));
     }
		public  void GenerateTransaction(String stock_symbol, String buyer_name, String seller_name)
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.sql.Date temp = java.sql.Date.valueOf("2005-02-08");
	//		java.sql.Date temp2 = java.sql.Date.valueOf("2005-02-08");
			Integer opid = 1;
			String option_type = "";

			try {
			Double trade_price_on_that_day = 0.0;
			Class.forName("com.mysql.jdbc.Driver");
           	Connection myconnect = DriverManager.getConnection(url, username, password);
	        Statement st = myconnect.createStatement();
	        ResultSet rs = st.executeQuery("SELECT TRADE_DATE, avg(TRADE_PRICE) as day_trade_price from STOCK_TRADE inner join options_detail on STOCK_TRADE.TRADING_SYMBOL = options_detail.ulying_stock where options_detail.ulying_stock = " + stock_symbol + " and STOCK_TRADE.TRADE_DATE = options_detail.issue_date group by TRADE_DATE");
	        while(rs.next()) {trade_price_on_that_day = rs.getDouble("day_trade_price"); }
	        ResultSet rs2 = st.executeQuery("SELECT issue_date, op_id, option_type from options_detail where ulying_stock = " + stock_symbol);
	        while (rs2.next()) {temp = rs2.getDate("issue_date"); opid = rs2.getInt("op_id"); option_type = rs2.getString("option_type");}
			String sqlquery = "INSERT INTO transactions(op_id, buyer, seller, trans_date, exercise_date, stock_price, option_type) values (?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement ps = myconnect.prepareStatement(sqlquery);
			ps.setInt(1, opid);
			ps.setString(2, buyer_name);
			ps.setString(3, seller_name);
			ps.setDate(4, temp);
			ps.setDate(5, AddExpTime(temp, 30));
			ps.setDouble(6, trade_price_on_that_day);
			ps.setString(7, option_type);
			ps.executeUpdate();
			
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		public void CreateTransactionsTable()
		{
			

		}
		public CreateTransactions()
		{
				try {

			Class.forName("com.mysql.jdbc.Driver");
           	Connection myconnect = DriverManager.getConnection(url, username, password);
	        Statement st = myconnect.createStatement();
	     //   st.execute("DROP INDEX opid");
	     //   st.execute("CREATE INDEX opid ON options_detail(op_id)");
	   //     st.execute("CREATE INDEX optype ON options_detail(option_type)");
	        st.execute(createtablesqlstatement);
	        for(int j = 0; j < modifiedstocksymbols.length; j++)
	        	GenerateTransaction(modifiedstocksymbols[j], "'Paul'", "'John'");

	    	}
	    	catch(Exception ex) {
	    		ex.printStackTrace();
	    	}
			

		}
}
