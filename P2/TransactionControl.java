import java.sql.*;
public class TransactionControl extends TransactionBase {

	private static void DropAllTables()
	{
		try{
			Class.forName("com.mysql.jdbc.Driver");
	           	Connection myconnect = DriverManager.getConnection(url, username, password);
		        Statement st = myconnect.createStatement();
		        st.execute("DROP TABLE IF EXISTS transactions");
		        st.execute("DROP TABLE IF EXISTS buyers_results");
		        st.execute("DROP TABLE IF EXISTS options_detail");
	    }
	    catch(Exception ex)
	    {
	    	ex.printStackTrace();
	    }
	}
	public static void main(String[] args)
	{
		DropAllTables();
		new CreateOptions();
		new CreateTransactions();
		new CreateBuyers();
	}
}