/* The 10 symbols being used are: AAA, AEU, BHT,
	 AZX, BML, AAW, BCW, AJQ, AGE, AGD */


/*This table contains the details of the options
that are being offered with and artificial key op_id */
CREATE INDEX stocksymobl ON INSTRUMENT(INSTR_NAME);
CREATE TABLE options_detail(
op_id int(11) NOT NULL AUTO_INCREMENT, 
ulying_stock varchar(30) DEFAULT NULL,
issue_date datetime,
duration int,
strike_price decimal,
premium decimal,
option_type varchar(7),
description varchar(140),
FOREIGN KEY(ulying_stock) REFERENCES INSTRUMENT(INSTR_NAME),
PRIMARY KEY(op_id)
)


/*This table contains the transactions and their
respective attributes for the underlying stock described
in the options table */
CREATE TABLE transactions(
op_id int(11) NOT NULL,
buyer varchar(140) NOT NULL,
seller varchar(140) NOT NULL,
trans_date datetime NOT NULL, --date transaction was carried out
exercise_date datetime, --date transaction expires
stock_price decimal,
PRIMARY KEY(buyer, seller, trans_date),
FOREIGN KEY(op_id) REFERENCES options_details(op_id)
)


/* This table holds the results of options that
have been exercised from buyer. */
CREATE TABLE buyers_results(
op_id int(11) NOT NULL,
result_id int(11) NOT NULL AUTO_INCREMENT,
result decimal,
gain tinyint(1), 
result_descrip varchar(7),
PRIMARY KEY(result_id),
FOREIGN KEY(op_id) REFERENCES options_detail(op_id)
)

ALTER TABLE buyers_results AUTO_INCREMENT=10


/*Create Put and Call Options */
/* For the Call option if stock price > strike price its a win for the buyer */
/* For the Put option if the stock price > strike price its a win for the buyer */

--1 Loss
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('AAA', NOW(), 3, 30, 2.50, "CALL")

--2 WIN
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('AEU', NOW(), 4, 42.00 , 1.00, "CALL")

--3 LOSS
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('BHT', NOW(), 2, 55.00, 2.00, "CALL")

--4  LOSS
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('AZX', NOW(), 5, 3.50, 0.50, "CALL")

--5 WIN
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('BML', NOW(), 6, 25.00, 1.50, "CALL")

--6  WIN
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('AAW', NOW(), 1, 51.00, 2.50, "PUT")

--7 WIN
INSERT INTO options_detail(ulying_stock,issue_date, duration, strike_price,
			premium, option_type) 
values ('BCW', NOW(), 10, 7.25, 0.25, "PUT")

--8 LOSS
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('AJQ',  NOW(), 8, 7.25, 0.25, "PUT")

--9 WIN
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('AGE', NOW(), 12, 29.00, 3.25, "PUT")

--10 LOSS
INSERT INTO options_detail(ulying_stock, issue_date, duration, strike_price,
			premium, option_type) 
values ('AGD', NOW(), 7, 19.00, 1.25, "PUT")


/*Populate Transaction table*/

--1 
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='AAA', "John", "Pam",
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail,
	22.00)

--2
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='AEU', "Seth", "Harry",
	SELECT DATE_ADD(issue_date, INTERVAL 2 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'AEU',
	43.96)

--3
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='BHT', "Tom", "Mary",
	SELECT DATE_ADD(issue_date, INTERVAL 1 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'BHT',
	51.00)

--4
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='AZX', "Hannah", "Paul",
	SELECT DATE_ADD(issue_date, INTERVAL 4 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'AZX',
	3.00)

--5
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='BML', "Taylor", "Peter",
	SELECT DATE_ADD(issue_date, INTERVAL 5 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'BML',
	28.15)

--6
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='AAW', "Ben", "Harris",
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'AAW',
	59.00)

--7
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='BCW', "Alice", "Jane",
	SELECT DATE_ADD(issue_date, INTERVAL 6 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'BCW',
	8.00)

--8
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='AJQ', "Ram", "Julia",
	SELECT DATE_ADD(issue_date, INTERVAL 7 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'AJQ',
	5.00)

--9
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='AGE', "James", "Johnson",
	SELECT DATE_ADD(issue_date, INTERVAL 10 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'AGE',
	33.00)

--10
INSERT INTO transactions (op_id, buyer, seller, trans_date, exercise_date, stock_price)
values (SELECT op_id from options_detail where ulying_stock='AGD', "Faith", "Tim",
	SELECT DATE_ADD(issue_date, INTERVAL 6 MINUTE) from options_detail, 
	SELECT DATE_ADD(issue_date, INTERVAL duration MINUTE) from options_detail where ulying_stock = 'AGD',
	19.00)

/* Stored Procedure to update Transactions Table
an example call to this table would be: 
CALL updateTransactions('AAA', 'John', 'Pam', SELECT DATE_ADD(issue_date,
INTERVAL duration MINUTE) from options_detail, SELECT DATE_ADD(issue_date,
INTERVAL duration MINUTE) from options_detail where ulying_stock='AEU',
SELECT TRADING_PRICE from STOCK_TRADE where TRADE_TIME=*ENTER TIME* AND TRADING_SYMBOL='AAA' */

DROP PROCEDURE IF EXISTS `updateTransactions` 
GO
DELIMITER //
CREATE PROCEDURE updateTransactions
(
    IN stocksym varchar(30), 
    IN pbuyer varchar(140) not null,
    IN pseller varchar(14) not null,
    IN ptrans_date datetime not null,
    IN pexercise_date datetime,
    IN pstock_price decimal
)
BEGIN
    INSERT INTO transactions
    (
        op_id, 
	buyer,
	seller,
	trans_date,
	exercise_date,
	stock_price,
    )
    VALUES
    (
	select op_id from options_details where ulying_stock=stocksym,
	pbuyer,
	pseller,
 	ptrans_date,
	pexercise_date,
	pstock_price,
    );
END //
DELIMITER;


/* Stored Procedure for update buyers results. Might have to use high level language
in order to check whether or not the transaction date has arrived (IF ptrans_date =
NOW(). Maybe by calling the procedure only when the transaction time has arrived and removing the condition checking this in the procedure */

DROP PROCEDURE IF EXISTS `updateResults` 
GO
DELIMITER //
CREATE PROCEDURE updateResults
(
    IN ptrans_date datetime not null,
    IN pexercise_date datetime,
)
BEGIN
    IF ptrans_date = NOW() THEN  
	IF (pexercise_date IS NOT NULL) THEN 
		INSERT INTO buyers_results(op_id, result, gain)
			select t1.op_id, (t1.strike_price + t1.premium)-t2.stock_price,
			       t2.stock_price>(t2.strike_price+t1.premium)
			from options_detail t1, transactions t2;
	ELSE
		INSERT INTO buyers_results(op_id, result, gain)
			select t.op_id, t1.premium, '0'
			from options_detail t1;
	end if; 
END //
DELIMITER;
