import java.sql.*;
//For below Dates
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
public class CreateOptions extends TransactionBase {
	//this string builds the options table

     private String BuildOptionsTable() 
     {
     	StringBuilder options = new StringBuilder();
     	options.append("CREATE TABLE options_detail( ");
     	options.append("op_id int(11) NOT NULL AUTO_INCREMENT, ");
     	options.append("ulying_stock varchar(3) NOT NULL,");
		options.append("issue_date date,");
		options.append("strike_price decimal (10, 2),");
		options.append("premium decimal (10, 2),");
		options.append("option_type varchar(7),");
		options.append("FOREIGN KEY(ulying_stock) REFERENCES INSTRUMENT(INSTR_NAME),");
		options.append("PRIMARY KEY(op_id)");
		options.append(")");
		return options.toString();
     }
     //checks if the current date is a weekend day or not. Stock market is not open on Sundays/Saturdays
     private String  CheckWeekend(SimpleDateFormat sdf, String date) throws ParseException
     {
     		Calendar c = Calendar.getInstance();
     		c.setTime(sdf.parse(date));
     		Integer dayofWeek = c.get(Calendar.DAY_OF_WEEK);
     		if(dayofWeek == 1) {c.add(Calendar.DATE,1); return sdf.format(c.getTime());}
     		else if(dayofWeek == 7) {c.add(Calendar.DATE, 2); return sdf.format(c.getTime());}
     		else {return date;}
     }
     private String CheckDate(String date) throws ParseException
     {
     		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
     		Date lowerbound = sdf.parse("2005-02-08");
     		Date upperbound = sdf.parse("2005-10-14");
     		Date the_date = sdf.parse(date);
     		if(the_date.compareTo(lowerbound) < 0) {return sdf.format(lowerbound);}
     		if(the_date.compareTo(upperbound) > 0) {return sdf.format(upperbound);} //We wish for our expiration dates to be 30 days apart from the original. This is the max date we'll need. 
     		return CheckWeekend(sdf, date);
     		
     }
     //use this function for inserting
     public void InsertOptions(String stock_symbol, String date, double strike_price, double premium, String type) 
     {
     		
     		try {

     		Class.forName("com.mysql.jdbc.Driver");
           	Connection myconnect = DriverManager.getConnection(url, username, password);
           	Statement st = myconnect.createStatement();
               //Prepared statement for controlling the dates
     		PreparedStatement ps1 = myconnect.prepareStatement("INSERT INTO options_detail(ulying_stock, issue_date, strike_price, premium, option_type) values (?, ?, ?, ?, ?)");
     		ps1.setString(1, stock_symbol);
     		ps1.setDate(2, java.sql.Date.valueOf(CheckDate(date)));
     		ps1.setDouble(3, strike_price);
     		ps1.setDouble(4, premium);
     		ps1.setString(5, type);
     		ps1.executeUpdate();
     		}
     		catch(Exception ex)
     		{
     			ex.printStackTrace();
     		}	
     		
     }
	public CreateOptions()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");

           	Connection myconnect = DriverManager.getConnection(url, username, password);
	        Statement st = myconnect.createStatement();
	      //  st.execute("DROP INDEX stockquote");
	       // st.execute("CREATE INDEX stockquote ON INSTRUMENT(INSTR_NAME)"); 
	        st.execute(BuildOptionsTable());
	        InsertOptions(stocksymbols[0], "2005-02-09", 30, 2.50, "'CALL'");
	        InsertOptions(stocksymbols[1], "2005-01-01", 45, 3.00, "'CALL'");
	        InsertOptions(stocksymbols[2], "2005-10-17", 6, 2.00, "'CALL'"); 
             InsertOptions(stocksymbols[3], "2005-09-03", 10, 3.00, "'CALL'");
             InsertOptions(stocksymbols[4], "2005-08-04", 11, 4.00, "'CALL'");
             InsertOptions(stocksymbols[5], "2005-07-03", 15, 5.00, "'PUT'");
             InsertOptions(stocksymbols[6], "2005-06-19", 18, 6.00, "'PUT'");
             InsertOptions(stocksymbols[7], "2005-07-13", 23, 5.00, "'PUT'");
             InsertOptions(stocksymbols[8], "2005-04-28", 24, 4.00, "'PUT'");
             InsertOptions(stocksymbols[9], "2005-02-15", 23, 3.00, "'PUT'");
	    }
	    catch(Exception ex) 
	    {
	    	ex.printStackTrace();
	    }
	}

}