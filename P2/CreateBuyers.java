import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.Random;
public class CreateBuyers extends TransactionBase {
	public CreateBuyers()
	{
	
     		try {
     			Class.forName("com.mysql.jdbc.Driver");
           		Connection myconnect = DriverManager.getConnection(url, username, password);
	        	Statement st = myconnect.createStatement();
	        	st.execute(BuyersTable());
	        	st.execute("ALTER TABLE buyers_results AUTO_INCREMENT = 10"); //sets the limit on amount of stocks to 10
	        	PreparedStatement ps = myconnect.prepareStatement("INSERT INTO buyers_results(winner) values (?)");

	        	for(int i =0; i< modifiedstocksymbols.length; i++)
	        	{
	        	Random rand = new Random();
	        	int y = rand.nextInt(2);
	        	if(y == 1) {
	        		
	        		ps.setString(1, "The winner of the PUT is " + DetermineWinner(modifiedstocksymbols[i], "PUT"));
	        		ps.executeUpdate();
	        	}
	        	else 
	        		{
	        			ps.setString(1, "The winner of the CALL is " + DetermineWinner(modifiedstocksymbols[i], "CALL"));
	        			ps.executeUpdate();
	        		}
	        	}

     		}
     		catch(Exception ex)
     		{
     			ex.printStackTrace();
     		}
	}
	public void InsertIntoBuyers(String stock_symbol)
	{
		Random rand = new Random();
		int x = rand.nextInt(1);
		int y = rand.nextInt(1); //Explanation: For some odd reason, SQL does not output the stock symbol correctly when running the command (Still don't know why....)
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date expiration_date = java.sql.Date.valueOf("2005-02-08");
		//So this is a temporary fix
		try { 
				Class.forName("com.mysql.jdbc.Driver");
           		Connection myconnect = DriverManager.getConnection(url, username, password);
				 Statement st = myconnect.createStatement();
	        	Integer op_id = 0;
	        	ResultSet opid = st.executeQuery("SELECT op_id from options_detail where ulying_stock = " + stock_symbol);

	        	while(opid.next()) {op_id = opid.getInt("op_id");}
	        	ResultSet exercise_date = st.executeQuery("SELECT exercise_date FROM transactions where op_id =" + opid);
	        	while(exercise_date.next()) {expiration_date = exercise_date.getDate("exercise_date");}
	        	PreparedStatement ps = myconnect.prepareStatement("INSERT INTO buyers(op_id, result, shares_bought, winner) values (?, ?, ?, ?)");
	        	ps.setInt(1, op_id);
	        	ps.setDouble(2, CalculateResults(stock_symbol, x, expiration_date));
	        	//Determine if we exercise the option or not
	        	if(x == 0) ps.setInt(3, 100);
	        	else ps.setInt(3, 0);
	        	if(y == 0) ps.setString(4, DetermineWinner(stock_symbol, "PUT"));
	        	else ps.setString(4, DetermineWinner(stock_symbol, "CALL"));

	        }
	        catch(Exception ex)
	        {
	        	ex.printStackTrace();
	        }
	}
	private Double CalculateResults(String stock_symbol, Integer rand_num, java.sql.Date expiration_date)
	{
		Double stock_price_on_expiration = 0.0;
		Double strike_price = 0.0;
		Double premium = 0.0; 
		Double results = 0.0;
			try {
				Class.forName("com.mysql.jdbc.Driver");
           		Connection myconnect = DriverManager.getConnection(url, username, password);
           		Statement st = myconnect.createStatement();
           		PreparedStatement ps = myconnect.prepareStatement("SELECT TRADE_DATE, avg(TRADE_PRICE) as day_trade_price from STOCK_TRADE inner join options_detail on STOCK_TRADE.TRADING_SYMBOL = options_detail.ulying_stock where options_detail.ulying_stock = " + stock_symbol + " and STOCK_TRADE.TRADE_DATE = ? group by TRADE_DATE");
				ps.setDate(1, expiration_date);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {stock_price_on_expiration = rs.getDouble("day_trade_price");}
				ResultSet rs2 = st.executeQuery("SELECT strike_price, premium from options_detail where ulying_stock = " + stock_symbol);
				while(rs2.next()) {strike_price = rs2.getDouble("strike_price"); premium = rs2.getDouble("premium");}
				if(rand_num == 0)
				{
					results = stock_price_on_expiration - strike_price - premium;
				}
				else results = -premium;


			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return results;
	}
	private String BuyersTable() 
	{
		StringBuilder buyers = new StringBuilder();
		buyers.append("CREATE TABLE buyers_results(");
		buyers.append("winner varchar(140)");
		buyers.append(")");
		return buyers.toString();
	}
	private String DetermineWinnerHelper(String stock_symbol, java.sql.Date expiration_date, Double strike_price, String option_type, String buyername, String sellername)
	{
		String winner = "";
		Double stock_price_on_expiration = 0.0;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
		Class.forName("com.mysql.jdbc.Driver");
           		Connection myconnect = DriverManager.getConnection(url, username, password);
	        	Statement st = myconnect.createStatement();
			 
			PreparedStatement ps = myconnect.prepareStatement("SELECT TRADE_DATE, avg(TRADE_PRICE) as day_trade_price from STOCK_TRADE inner join options_detail on STOCK_TRADE.TRADING_SYMBOL = options_detail.ulying_stock where options_detail.ulying_stock = " + stock_symbol + " and STOCK_TRADE.TRADE_DATE = ? group by TRADE_DATE");
			ps.setDate(1, expiration_date);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {stock_price_on_expiration = rs.getDouble("day_trade_price");}
		//	winner = stock_price_on_expiration.toString();
			//winner = option_type;	
			if(option_type == "PUT") //For a put, we determine that if on the expiration date, the strike price is greater than the current stock, the seller wins
			{
				if(strike_price > stock_price_on_expiration) winner = "The Seller " + sellername;
				else winner = "The Buyer " + buyername;
				
			}
			else if(option_type == "CALL") //For a call, we determine that if on the expiration date, the strike price is less than the current stock, the buyer wins
			{
				if(strike_price < stock_price_on_expiration) winner = "The buyer " + buyername;
				else return winner = "The seller" + sellername;
			}
			else {winner = "No winner";}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return winner; 
		
	}
	private String DetermineWinner(String stock_symbol, String option_type)
	{
		//String option_type = "";
		String buyer_name = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    java.sql.Date expiration_date = java.sql.Date.valueOf("2005-02-08");
	    String winner = "";
		try {
     			Class.forName("com.mysql.jdbc.Driver");
           		Connection myconnect = DriverManager.getConnection(url, username, password);
	        	Statement st = myconnect.createStatement();
	        	Double strike_price = 0.0;
	        	//String option_type = "";
	        	
	        	String seller_name = "";
	        	Integer op_id = 1;
	        	
	        	ResultSet strike_result = st.executeQuery("SELECT op_id, strike_price, option_type FROM options_detail where ulying_stock =" + stock_symbol);
	        	while(strike_result.next()) 
	        		{
	        			op_id = strike_result.getInt("op_id");
	        			strike_price = strike_result.getDouble("strike_price"); 
	        			
	        		}
	        	ResultSet exercise_date = st.executeQuery("SELECT exercise_date, buyer, seller FROM transactions where op_id =" + op_id);
	        	while(exercise_date.next()) 
	        	{
	        	expiration_date = exercise_date.getDate("exercise_date"); 
	        	buyer_name = exercise_date.getString("buyer"); 
	        	seller_name = exercise_date.getString("seller");
	   
	        	}
	        	winner = DetermineWinnerHelper(stock_symbol, expiration_date, strike_price, option_type, buyer_name, seller_name);	

     		}
     		catch(Exception ex)
     		{
     			ex.printStackTrace();	
     		}
     		return winner; 
     		
	}
}