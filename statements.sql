/*Get titles with more than one author. May not be necessary */
SELECT au_id, title_id
FROM titleauthor t1
WHERE EXISTS (
              SELECT *
              FROM titleauthor t2
              WHERE t1.title_id = t2.title_id
              HAVING COUNT(*) > 1
              );


/* Get the publishers that at least one these authors have worked with */
SELECT * from (SELECT au_id, title_id FROM titleauthor GROUP BY title_id) t1
INNER JOIN
	(SELECT title_id, pub_id  FROM titles GROUP BY title_id) t2
		ON t1.title_id = t2.title_id;


/* Insert new book into title relations.This must only be executed once for each title being added */
INSERT INTO  titles(title_id, title, type, pub_id, price, advance, total_sales, notes, pub_date, contract, ag_id)
	 VALUES ("title_id", "title", "type", "pub_id", price, advance, total_sales, notes, pub_data, contract, ag_id);


/* Insert  new book into titleauthor relations. This mmust be executed for each author wworking on the same book*/
INSERT INTO  titleauthor(au_id, title_id, au_ord, royaltyper)
	VALUES ("au_id", "title_id", au_ord, royaltyper);


/* Get book stores that stock books from the authors of the new book*/
SELECT au_id, stor_id from (SELECT stor_id, title_id FROM salesdetail GROUP BY title_id) t1
INNER JOIN
	(SELECT title_id, au_id FROM titleauthor GROUP BY title_id) t2
		ON t1.title_id = t2.title_id and t2.au_id ="au_id";

/* Insert new order at stores that stock books for the authors. This must be executed for each store that has sold book for any of the authors */
INSERT INTO salesdetail(stor_id, ord_num, title_id, qty, discount)
	VALUES(stor_id, ord_num, "title_id", qty, discount);


/*Get pending orders for the new book*/
SELECT * FROM pending_orders WHERE title_id = "title_id";


/* Get salesdetails and sales records*/
SELECT salesdetail.stor_id, salesdetail.ord_num, salesdetail.title_id, salesdetail.qty, salesdetail.discount FROM salesdetail
        JOIN sales ON (salesdetail.stor_id = sales.stor_id) and (salesdetail.title_id = "title_id")


/*Set pending orders for this book to fullfilled*/
UPDATE pending_orders SET fullfilled = 1 WHERE title_id = "title_id" 


/*Update book store inventories */
UPDATE store_inventories s INNER JOIN pending_orders p ON s.title_id = p.title_id SET s.qty = (s.qty - p.qty)


/*Insert new review*/
INSERT INTO reviews(review_id, title_id, periodical_id, rev_date, rating, content)
	VALUES("review_id", "title_id", "periodical_id", rev_date, rating, "content")


